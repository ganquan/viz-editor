import Vue from 'vue'
import '@/assets/icon/iconfont.css'
import App from './App'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
